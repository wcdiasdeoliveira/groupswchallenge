package br.com.groupswchallenge.ex1.model;

public class Triangle extends GeometricFigure {
	
	public Triangle(double b, double h) {
		super(b, h);
	}

	@Override
	public double getArea() {
		double area = 0.0;
		try {
			area = super.getArea() / 2;
		}catch (Exception ignored) {}
		return area;
	}
}
package br.com.groupswchallenge.ex1.calc;

import java.util.ArrayList;

import br.com.groupswchallenge.ex1.model.GeometricFigure;

public class AreaUtil {
	
	public static double sumArea(ArrayList<GeometricFigure> toSum) {
		double sum = 0.0;
		try {
			if(toSum != null && !toSum.isEmpty()) {
				sum = toSum.stream().map(element->element.getArea()).mapToDouble(Double::doubleValue).sum();
			}
		}catch (Exception ignored) {}
		return sum;
	}
}
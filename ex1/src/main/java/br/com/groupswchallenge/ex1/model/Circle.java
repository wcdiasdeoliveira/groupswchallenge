package br.com.groupswchallenge.ex1.model;

public class Circle extends GeometricFigure {
	
	private double radius;
	
	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public Circle(double radius) {
		super(0.0, 0.0);
		this.setRadius(radius);
	}

	@Override
	public double getArea() {
		return Math.PI * Math.pow(this.getRadius(), 2);
	}
}
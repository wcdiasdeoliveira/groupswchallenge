package br.com.groupswchallenge.ex1.model;

public abstract class GeometricFigure{
	
	private double b;
	private double h;
	
	public GeometricFigure(double b, double h) {
		this.setB(b);
		this.setH(h);
	}
	
	public double getB() {
		return b;
	}

	public void setB(double b) {
		this.b = b;
	}

	public double getH() {
		return h;
	}

	public void setH(double h) {
		this.h = h;
	}
	
	public double getArea() {
		double area = 0.0;
		try {
			area = this.getB() * this.getH();
		}catch (Exception ignored) {}
		return area;
	}
}
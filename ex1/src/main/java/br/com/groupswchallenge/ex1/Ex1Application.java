package br.com.groupswchallenge.ex1;

import java.util.ArrayList;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.com.groupswchallenge.ex1.calc.AreaUtil;
import br.com.groupswchallenge.ex1.model.Circle;
import br.com.groupswchallenge.ex1.model.GeometricFigure;
import br.com.groupswchallenge.ex1.model.Square;
import br.com.groupswchallenge.ex1.model.Triangle;
import br.com.groupswchallenge.ex1.model.Rectangle;

@SpringBootApplication
public class Ex1Application {

	public static void main(String[] args) {
		SpringApplication.run(Ex1Application.class, args);
		ArrayList<GeometricFigure> toCalc = new ArrayList<GeometricFigure>();
		toCalc.add(new Square(17, 17));
		toCalc.add(new Triangle(5, 12));
		toCalc.add(new Rectangle(10, 5));
		toCalc.add(new Circle(15));
		toCalc.forEach(element->System.out.println(element.getClass().getName() +  " -> area: " + element.getArea()));
		System.out.println(AreaUtil.sumArea(toCalc));
	}
}
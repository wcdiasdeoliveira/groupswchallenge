package br.com.groupswchallenge.ex3.model;

public enum Nature {
	
	COMMON(1), HIGH_RISK(2);
	
	public int identifier;
		 
    Nature(int id) {
    	identifier = id;
    }
}
package br.com.groupswchallenge.ex3.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.groupswchallenge.ex3.model.Infringement;

public interface InfringementRepository extends CrudRepository<Infringement, Long>{

}
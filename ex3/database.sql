CREATE TABLE agent(
	id INT NOT NULL AUTO_INCREMENT,	
	name VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)
)

CREATE TABLE driver(
	id INT NOT NULL AUTO_INCREMENT,	
	name VARCHAR(255) NOT NULL,
	license VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)
)

CREATE TABLE vehicle(
	id INT NOT NULL AUTO_INCREMENT,	
	plate VARCHAR(255) NOT NULL,
	model VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)
)

CREATE TABLE owner(
	id INT NOT NULL AUTO_INCREMENT,	
	name VARCHAR(255) NOT NULL,
	ssn VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)
)

CREATE TABLE infringement(
	id INT NOT NULL AUTO_INCREMENT,	
	nature INT NOT NULL,
	occurred DATETIME NOT NULL,
	local VARCHAR(255) NOT NULL,
	agent INT(3) NOT NULL,
	driver INT(3) NOT NULL,
	vehicle INT(3) NOT NULL,
	owner INT(3) NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(agent) REFERENCES agent(id),
	FOREIGN KEY(driver) REFERENCES driver(id),
	FOREIGN KEY(vehicle) REFERENCES vehicle(id),
	FOREIGN KEY(owner) REFERENCES owner(id)
);

INSERT INTO agent(name) VALUES("Agent");
INSERT INTO driver(name, license) VALUES("Agent", "X1");
INSERT INTO vehicle(plate, model) VALUES("XX1", "XX1");
INSERT INTO owner(name, ssn) VALUES("Owner", "XX1");
INSERT INTO infringement(nature, occurred, local, agent, driver, vehicle, owner) VALUES(
1, CURRENT_TIMESTAMP, "Here", 1, 1, 1 , 1);

package br.com.groupswchallenge.ex2.model;

import java.util.ArrayList;

public class ApartmentBlock {
	
	private ArrayList<Apartment> apartments;
	private double income;
	private double outcome;
	
	public ArrayList<Apartment> getApartments() {
		return apartments;
	}

	public void setApartments(ArrayList<Apartment> apartments) {
		this.apartments = apartments;
	}

	public double getIncome() {
		return income;
	}

	public void setIncome(double income) {
		this.income = income;
	}

	public double getOutcome() {
		return outcome;
	}

	public void setOutcome(double outcome) {
		this.outcome = outcome;
	}

	public ApartmentBlock(ArrayList<Apartment> apartments) {
		this.setApartments(apartments);
	}
}
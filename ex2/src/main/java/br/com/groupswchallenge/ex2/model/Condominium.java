package br.com.groupswchallenge.ex2.model;

import java.util.ArrayList;

public class Condominium {
	
	private ArrayList<ApartmentBlock> blocks;

	public ArrayList<ApartmentBlock> getBlocks() {
		return blocks;
	}

	public void setBlocks(ArrayList<ApartmentBlock> blocks) {
		this.blocks = blocks;
	}
	
	public Condominium(ArrayList<ApartmentBlock> blocks) {
		this.setBlocks(blocks);
	}
}
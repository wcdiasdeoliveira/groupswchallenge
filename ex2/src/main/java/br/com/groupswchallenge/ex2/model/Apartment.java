package br.com.groupswchallenge.ex2.model;

public class Apartment {
	
	private double area;
	private double tax;
	
	public double getArea() {
		return area;
	}

	public void setArea(double area) {
		this.area = area;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public Apartment(double area) {
		this.setArea(area);
	}
	
	public double calcTax(ApartmentBlock block) {
		double tax = 0.0;
		try {
			tax = ( block.getOutcome() / 100 ) * this.getArea(); 
		}catch (Exception ignored) {
			tax = 0.0;
		}
		this.setTax(tax);
		return this.getTax();
	}
}
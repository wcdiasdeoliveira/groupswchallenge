package br.com.groupswchallenge.ex2.calc;

import java.util.ArrayList;

import br.com.groupswchallenge.ex2.model.Apartment;
import br.com.groupswchallenge.ex2.model.ApartmentBlock;

public class CalcUtil {

	public static double calcTaxes(ArrayList<Apartment> toCalc,ApartmentBlock apartmentBlock ) {
		double taxes = 0.0;
		try {
			if(toCalc != null && !toCalc.isEmpty()) {
				taxes = toCalc.stream().map(element->element.calcTax(apartmentBlock)).mapToDouble(Double::doubleValue).sum();
			}
		}catch (Exception ignored) {}
		return taxes;
	}
}

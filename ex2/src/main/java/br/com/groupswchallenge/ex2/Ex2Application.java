package br.com.groupswchallenge.ex2;

import java.util.ArrayList;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.com.groupswchallenge.ex2.calc.CalcUtil;
import br.com.groupswchallenge.ex2.model.Apartment;
import br.com.groupswchallenge.ex2.model.ApartmentBlock;
import br.com.groupswchallenge.ex2.model.Condominium;

@SpringBootApplication
public class Ex2Application {

	public static void main(String[] args) {
		SpringApplication.run(Ex2Application.class, args);
		ArrayList<Apartment> apartments = new ArrayList<Apartment>();
		apartments.add(new Apartment(80.0));
		apartments.add(new Apartment(20.0));
		ApartmentBlock apartmentBlock = new ApartmentBlock(apartments);
		apartmentBlock.setOutcome(110.00);
		ArrayList<ApartmentBlock> blocks = new ArrayList<ApartmentBlock>();
		blocks.add(apartmentBlock);
		@SuppressWarnings("unused")
		Condominium condominium = new Condominium(blocks);
		apartments.forEach(element->System.out.println(element.getClass().getName() +  " -> tax: " + element.calcTax(apartmentBlock)));
		System.out.println(CalcUtil.calcTaxes(apartments, apartmentBlock));

	}
}